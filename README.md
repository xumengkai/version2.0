# datafusion 2.0

ST 多维数据融合平台 1.支持换肤 2.界面风格改造后的精简版 3.界面风格改造后的全功能版

## Vue 3.2 + Vite 2 + Element-Plus + Echarts 5

## 文件夹及文件命名规范

```
1.index.js 或者 index.vue，统一使用小写字母开头的(kebab-case)命名规范
2.除index.vue之外，其他.vue文件统一用PascalBase风格
3.文件夹名称统一使用小写字母开头的(kebab-case)命名规范

```

## 开发注意事项

```
1.开发过程中所有的input输入框要做类型校验，控制输入内容不得含有'<'或'>'。严格控制输入类型（防止xss漏洞攻击）


```

## git 设置全局:

```bash

cd existing_repo
git config --global user.name "Your Name"
git config --global user.email "you@example.com"
git remote remove origin
git remote add origin https://gitlab.com/xumengkai/version2.0.git
git config --global http.sslVerify false
git push --set-upstream origin main

```

## 配置说明：

```

1. Vue 3.2 + Vite 2 + Element-Plus + Echarts 5 + Vue-router + Vuex

```

## 功能说明：

```

1.主题切换,一键换肤 ✔
2.前端权限（动态路由、指令权限、权限函数）✔
3.自适应页面配置（样式 + 字体）
4.公共组件开发
5.scss公用样式、函数、参数复用 ✔
6.scss主题修改 ✔
7.路由守卫
8.SVG
9.在主页面做自适应布局，其他子页面放在主页面的盒子里（一次布局，全局使用）
10.真实图表(Echarts)绘制操作务必放在数据请求赋值之后操作
11.组件销毁时要重置（销毁）图表
12.为了提高用户体验感，强烈建议为图表容器加上loading状态

```

## 公共组件：

```

1.开发公共表单控件，带有分页栏（表格高度，分页栏是否显示、位置、最大显示按钮）
2.开发公共工具栏（搜索要素）控件（思路：没有获取到光标的时候长度缩小，获取到光标长度变长）
3.开发公共下载控件（下载格式可选）
4.开发公共title控件（自定义title长度）
5.开发公共echart图表（图表格式可选）

```

## 技术要点：

```javascript

1.css中v-bind使用v-bind() in CSS
    <template>
        <div class="text">hello</div>
    </template>

    <script setup>
        import { reactive } from 'vue'
        const state = reactive({
            color:red,
        })
    </script>

    <style>
        .text {
            color: v-bind('state.color');
        }
    </style>

2.setup 语法糖使用
    <script setup>
        import { ref, watch } from 'vue'
    </script>
    <template>

    </template>
    <style lang="scss" scoped>

    </style>

3.scss混合器中的CSS规则及传参
        @mixin no-bullets {
            li {
                list-style-image: none;
            }
        }
        @mixin link-colors($normal, $hover, $visited) {
            color: $normal;
            &:hover { color: $hover; }
            &:visited { color: $visited; }
        }

4.setup语法糖中父组件调用子组件中的方法，子组件需主动暴露出来，父组件才可调用 defineExpose
    <!-- 子组件 child.vue -->
    <script setup>
        const childMethod = () => {
            console.log('child method.')
        }

        // 主动暴露childMethod方法
        defineExpose({ childMethod })
    </script>

    <!-- 父组件 -->
    <script setup>
        import { ref } from 'vue'
        import child from './child.vue'

        const childRef = ref()

        const callChildMethod = () => {
        childRef.value.childMethod()
        }
    </script>

    <template>
    <child ref="childRef"></child>
    <button @click="callChildMethod">call</button>
    </template>


5.<script setup> 父子组件传值，使用 defineProps 和 defineEmits API 来替代 props 和 emits。

   （父组件向子组件传参）：
    //父组件
    <template>
         <div>父组件</div>
         <Child :title="msg" />
    </template>
    ​
    <script setup>
        import {ref} from 'vue'
        import Child from './child.vue'
        const msg = ref('父的值')  //自动返回，在template直接解套使用
    </script>

    //子组件
     <template/> 中可以直接使用父组件传递的props （可省略props.）
    <script-setup> 中需要通过props.xx获取父组件传递过来的props

    <template>
         <div>子组件</div>
         <div>父组件传递的值：{{title}}</div>
    </template>
    ​
    <script setup>
        //import {defineProps} from 'vue'   不需要引入
        ​
        //语法糖必须使用defineProps替代props
        const  props = defineProps({
             title: {
               type: String,
               default:''
            }
        });

        //script-setup中需要通过props.xx获取父组件传递过来的props
        console.log(props.title) //父的值
    </script>

    子组件向父组件传递数据（子组件向外暴露数据）：

    //子组件
    <template>
         <div>子组件</div>
         <button @click="toEmits">子组件向外暴露数据</button>
    </template>
    ​
    <script setup>
        import {ref} from 'vue'
        const name = ref('我是子组件')

        const  emits = defineEmits(['childFn']);//1、暴露内部数据
        ​
        const  toEmits = () => {
             emits('childFn',name) //2、触发父组件中暴露的childFn方法并携带数据
        }
    </script>

    //父组件
    <template>
         <div>父组件</div>
         <Child  @childFn='childFn' />
         <p>接收子组件传递的数据{{childData}} </p>
    </template>
    ​
    <script setup>
        import {ref} from 'vue'
        import Child from './child.vue'
           
        const childData = ref(null)    
        const childFn=(e)=>{
           consloe.log('子组件触发了父组件childFn，并传递了参数e')
           childData=e.value
        }    
         
    </script>
​

​

​

6.自适应布局


```
