//样式自动给css添加兼容性前缀
module.exports = {
	plugins: [require('autoprefixer')],
	autoprefixer: {
		// 兼容浏览器，添加前缀
		overrideBrowserslist: ['Android 4.1', 'iOS 7.1', 'Chrome > 31', 'ff > 31', 'ie >= 8'],
		grid: true
	}
}
