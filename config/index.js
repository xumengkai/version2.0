// 环境配置
export default {
	dev: {
		domain: {
			'/st_api': {
				target: 'http://61.177.139.106:5672/st_api/',
				changeOrigin: true,
				ws: true,
				rewrite: (path) => path.replace(/^\/st_api/, '')
			},
			'/pvd': {
				target: 'http://61.177.139.106:5672/pvd/',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/pvd/, '')
			},
			'/ws_st_api': {
				target: 'ws://61.177.139.106:5672/st_api/',
				ws: true,
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/ws_st_api/, '')
			}
		},
		port: '7000',
		mock: true
	},
	test: {
		domain: {
			'/api': {
				target: 'http://192.168.100.192:18003/',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/api/, '')
			}
		},
		port: '3001',
		mock: false
	},
	pre: {
		domain: 'http://61.177.139.106:5672/',
		port: '3000',
		mock: true
	},
	prod: {
		domain: 'http://68.168.75.92:5672/',
		port: '3000',
		mock: true
	}
}
