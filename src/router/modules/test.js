const test = [
	{
		path: '/test/test1',
		name: 'Test1',
		component: () => import('@/views/test/Index1.vue'),
		meta: {
			title: '测试一'
		}
	},
	{
		path: '/test/test2',
		name: 'Test2',
		component: () => import('@/views/test/Index2.vue'),
		meta: {
			title: '测试二'
		}
	},
	{
		path: '/test/test3',
		name: 'Test3',
		component: () => import('@/views/test/Index3.vue'),
		meta: {
			title: '测试三'
		}
	},
	{
		path: '/test2/test4',
		name: 'Test4',
		component: () => import('@/views/test/Index4.vue'),
		meta: {
			title: '测试四'
		}
	},
	{
		path: '/test3/test5',
		name: 'Test5',
		component: () => import('@/views/test/Index5.vue'),
		meta: {
			title: 'ECharts'
		}
	}
]

export default test
