import { createRouter, createWebHashHistory } from 'vue-router'
import store from '@store'
import utils from '@utils'
import _ from 'lodash'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const { localGet, setPageTitle } = utils

//路由模块
import test from './modules/test'

const routes = [
	{
		path: '',
		name: '首页',
		meta: {},
		component: () => import('@/views/common/Home.vue'),
		children: [
			{
				path: '/no-auth',
				meta: {
					title: '暂无权限',
					isHide: false
				},
				component: () => import('@/components/404.vue')
			}
		]
	},
	{
		path: '/login',
		name: 'Login',
		component: () => import('@/views/login/Index.vue'),
		meta: {
			title: '登录'
		}
	},
	{
		path: '/home',
		name: 'Home',
		component: () => import('@/views/common/Home.vue'),
		meta: {
			title: '首页'
		}
	},
	{
		path: '/404',
		name: '404',
		meta: {
			title: '404',
			isHide: false
		},
		component: () => import('@/components/404.vue')
	},
	...test,
	{
		path: '/:pathMatch(.*)*',
		redirect: '/404'
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

// 权限检查
const asyncGetAuthFun = (to) => {
	return new Promise((resolve, reject) => {
		if (!localGet('token') && to.path !== '/login') {
			resolve(false)
		} else {
			resolve(true)
		}
	})
}
// 动态路由
const hasNecessaryRoute = () => {
	return router.getRoutes().find((v) => v.path === '')['children'].length !== 2
}
// 生产路由
// const _import = (file) => require('@/' + file).default
const generateRoute = () => {
	const menus = _.cloneDeep(store.state.user.menus)
	// 获取vuex中的路由
	const getMenus = (menus) => {
		menus.forEach((item) => {
			item.component = import('@/' + item.component /* @vite-ignore */)
			if (item.children) {
				getMenus(item.children)
			}
		})
	}
	getMenus(menus)
	router.addRoute({
		path: '',
		meta: {},
		redirect: menus[0]['path'],
		component: () => import('@/views/common/Home.vue'),
		name: 'Home',
		children: [...menus, ...routes[0]['children']]
	})
}

//路由守卫
router.beforeEach(async (to, from) => {
	try {
		NProgress.start()
		const res = await asyncGetAuthFun(to)
		document.title = setPageTitle(to.meta.title) //设置页面title
		if (!res) {
			return '/login'
		} else if (to.path !== '/login' && !hasNecessaryRoute()) {
			generateRoute()
			if (to.redirectedFrom) {
				return to.redirectedFrom.fullPath
			} else {
				return to.fullPath
			}
		} else {
			if (to.name == 'Home') {
				store.dispatch('user/removeMenusRouter')
			}
			NProgress.done()
			return true
		}
	} catch (error) {
		if (error instanceof NotAllowedError) {
			return false
		} else {
			throw error
		}
	}
})

//路由错误回调
router.onError((handler) => {
	NProgress.done()
	console.log('路由错误:', handler)
})

export default router
