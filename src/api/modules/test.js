import axios from '../axios'
const prefix = axios.prefix

//测试
export const logintest = (data) => {
	return axios.post(`${prefix}/auth/login`, data)
}

//选择系统
export const selectSystem = (data) => {
	return axios.post(`${prefix}/auth/selectSystem`, data)
}
