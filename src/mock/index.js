export default [
	{
		url: '/st_api/auth/login',
		method: 'post',
		response: () => {
			return {
				info: null,
				isSuccess: true,
				success: true,
				data: {
					id: '100010101',
					name: 'admin',
					realname: '超级管理员',
					password: 'e10adc3949ba59abbe56e057f20f883e',
					status: 1,
					sessionId: 'f2183af2-3bd5-42e5-8ac8-63733dc762f2',
					roles: [
						{
							id: '100010101',
							name: '超级管理员'
						}
					],
					groups: [
						{
							id: '1',
							name: '神探电子',
							code: '001'
						}
					],
					router: [
						{
							path: '/home',
							name: 'Home',
							component: 'components/Template.vue',
							children: [
								{
									path: '',
									meta: {
										title: '首页'
									},
									component: 'views/common/Home.vue'
								}
							],
							meta: {
								title: '首页',
								icon: 'house',
								isHideSubMenu: true,
								isAuth: true
							}
						},
						{
							path: '/test',
							redirect: '/test/test1',
							meta: {
								title: '测试组一',
								icon: 'AddLocation',
								isAuth: true
							},
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							children: [
								{
									path: 'test1',
									meta: {
										title: '1-1'
									},
									component: 'views/test/Index1.vue'
								},
								{
									path: 'test2',
									meta: {
										isHide: false,
										title: '1-2'
									},
									component: 'views/test/Index2.vue'
								},
								{
									path: 'test3',
									meta: {
										isHide: false,
										title: '1-3'
									},
									component: 'views/test/Index3.vue'
								}
							]
						},
						{
							path: '/test2',
							redirect: '/test2/test4',
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							meta: {
								title: '测试组二',
								icon: 'Message',
								isAuth: true
							},
							children: [
								{
									path: 'test4',
									meta: {
										title: '2-1'
									},
									component: 'views/test/Index1.vue'
								},
								{
									path: 'test5',
									meta: {
										isHide: true,
										title: '2-2'
									},
									component: 'views/test/Index2.vue'
								},
								{
									path: 'test6',
									meta: {
										isHide: false,
										title: '2-3'
									},
									component: 'views/test/Index3.vue'
								}
							]
						},
						{
							path: '/test3',
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							meta: {
								title: '测试组三',
								icon: 'Operation',
								isAuth: true
							}
						}
					]
				}
			}
		}
	},
	{
		url: '/st_api/auth/selectSystem',
		method: 'post',
		response: () => {
			return {
				info: null,
				isSuccess: true,
				success: true,
				data: {
					router: [
						{
							path: '/home',
							name: 'Home',
							component: 'components/Template.vue',
							children: [
								{
									path: '',
									meta: {
										title: '平台首页'
									},
									component: 'views/common/Home.vue'
								}
							],
							meta: {
								title: '平台首页',
								icon: 'HomeFilled',
								isHideSubMenu: true,
								isAuth: true
							}
						},
						{
							path: '/test',
							redirect: '/test/test1',
							meta: {
								title: '系统管理',
								icon: 'DataLine',
								isAuth: true
							},
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							children: [
								{
									path: 'test1',
									meta: {
										title: '用户管理'
									},
									component: 'views/test/Index1.vue'
								},
								{
									path: 'test2',
									meta: {
										isHide: false,
										title: '组织管理'
									},
									component: 'views/test/Index2.vue'
								},
								{
									path: 'test3',
									meta: {
										isHide: false,
										title: '权限管理'
									},
									component: 'views/test/Index3.vue'
								}
							]
						},
						{
							path: '/test2',
							redirect: '/test2/test4',
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							meta: {
								title: '性能分析',
								icon: 'LocationInformation',
								isAuth: true
							},
							children: [
								{
									path: 'test4',
									meta: {
										title: '基础数据查询'
									},
									component: 'views/test/Index1.vue'
								},
								{
									path: 'test5',
									meta: {
										isHide: true,
										title: '统计分析'
									},
									component: 'views/test/Index2.vue'
								},
								{
									path: 'test6',
									meta: {
										isHide: false,
										title: '数据导出'
									},
									component: 'views/test/Index3.vue'
								}
							]
						},
						{
							path: '/test3',
							auths: ['add', 'view', 'delete', 'edit'],
							component: 'components/Template.vue',
							meta: {
								title: '设备管理',
								icon: 'Platform',
								isAuth: true
							},
							children: [
								{
									path: 'test5',
									meta: {
										title: 'ECharts图例'
									},
									component: 'views/test/Index5.vue'
								}
							]
						}
					]
					// router: [
					// 	{
					// 		path: '/home',
					// 		name: 'Home',
					// 		component: 'components/Template.vue',
					// 		children: [{
					// 			path: '',
					// 			meta: {
					// 				title: '首页'
					// 			},
					// 			component: 'views/common/Home.vue'
					// 		}],
					// 		meta: {
					// 			title: '平台首页',
					// 			icon: 'house',
					// 			isHideSubMenu: true,
					// 			isAuth: true
					// 		}
					// 	},
					// 	{
					// 		path: '/test',
					// 		redirect: '/test/test1',
					// 		meta: {
					// 			title: '工作舱',
					// 			icon: 'AddLocation',
					// 			isAuth: true
					// 		},
					// 		auths: ['add', 'view', 'delete', 'edit'],
					// 		component: 'components/Template.vue',
					// 		children: [{
					// 				path: 'test1',
					// 				meta: {
					// 					title: '车码融合'
					// 				},
					// 				component: 'views/test/Index1.vue'
					// 			},
					// 			{
					// 				path: 'test2',
					// 				meta: {
					// 					isHide: false,
					// 					title: '伴随分析'
					// 				},
					// 				component: 'views/test/Index2.vue'
					// 			},
					// 			{
					// 				path: 'test3',
					// 				meta: {
					// 					isHide: false,
					// 					title: '碰撞分析'
					// 				},
					// 				component: 'views/test/Index3.vue'
					// 			}
					// 		]
					// 	},
					// 	{
					// 		path: '/test2',
					// 		redirect: '/test2/test4',
					// 		auths: ['add', 'view', 'delete', 'edit'],
					// 		component: 'components/Template.vue',
					// 		meta: {
					// 			title: '驾驶舱',
					// 			icon: 'Message',
					// 			isAuth: true
					// 		},
					// 		children: [{
					// 				path: 'test4',
					// 				meta: {
					// 					title: '中间库'
					// 				},
					// 				component: 'views/test/Index1.vue'
					// 			},
					// 			{
					// 				path: 'test5',
					// 				meta: {
					// 					isHide: true,
					// 					title: '统计分析'
					// 				},
					// 				component: 'views/test/Index2.vue'
					// 			},
					// 			{
					// 				path: 'test6',
					// 				meta: {
					// 					isHide: false,
					// 					title: '数据导出'
					// 				},
					// 				component: 'views/test/Index3.vue'
					// 			}
					// 		]
					// 	},
					// 	{
					// 		path: '/test3',
					// 		auths: ['add', 'view', 'delete', 'edit'],
					// 		component: 'components/Template.vue',
					// 		meta: {
					// 			title: '案件管理',
					// 			icon: 'Operation',
					// 			isAuth: true
					// 		}
					// 	}
					// ]
				}
			}
		}
	}
]
