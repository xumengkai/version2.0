//样式模块
const state = {
	menusSytle: {} //菜单样式
}

const mutations = {
	SET_MENUSSTYLE: (state, menusSytle) => {
		state.menusSytle = menusSytle
	}
}

const actions = {
	setMenusInfo: ({ commit }, menusInfo) => {
		const { muensStyle } = menusInfo
		commit('SET_MENUSSTYLE', muensStyle)
	},
	removeMenusInfo: ({ commit }) => {
		commit('SET_MENUSSTYLE', {})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
