//用户模块
const state = {
	userInfo: {}, //用户信息
	token: '', //token
	roles: [], // 用户权限
	menus: [], //菜单组
	system: '' //选择的系统
}

const mutations = {
	SET_USERINFO: (state, userInfo) => {
		state.userInfo = userInfo
	},
	SET_TOKEN: (state, token) => {
		state.token = token
	},
	SET_ROLES: (state, roles) => {
		state.roles = roles
	},
	SET_MENUS: (state, menus) => {
		state.menus = menus
	},
	SET_SYSTEM: (state, system) => {
		state.system = system
	}
}

const actions = {
	setUserInfo: ({ commit }, userInfo) => {
		const { sessionId, roles, router } = userInfo
		commit('SET_USERINFO', userInfo)
		commit('SET_TOKEN', sessionId) // vuex存储token
		commit('SET_ROLES', roles) // vuex存储roles
		// commit('SET_MENUS', router)
	},
	setSystem: ({ commit }, systemData) => {
		commit('SET_SYSTEM', systemData)
	},
	removeUserInfo: ({ commit }) => {
		commit('SET_USERINFO', {})
		commit('SET_TOKEN', '') // 清除token
		commit('SET_ROLES', []) // 清除roles
		commit('SET_MENUS', [])
		commit('SET_SYSTEM', '')
	},
	getMenusRouter: ({ commit }, data) => {
		const { router } = data
		commit('SET_MENUS', router)
	},
	removeMenusRouter: ({ commit }) => {
		commit('SET_MENUS', [])
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
