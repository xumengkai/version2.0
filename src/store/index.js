import { createStore } from 'vuex'
import getters from './getters.js'
import persistedState from 'vuex-persistedstate'
const modulesFiles = import.meta.globEager('./modules/*.js')

let modules = {}

for (const path in modulesFiles) {
	const moduleName = path.replace(/(.*\/)*([^.]+).*/gi, '$2')
	modules[moduleName] = modulesFiles[path].default
}

const store = new createStore({
	modules,
	getters,
	// plugins: [persistedState({ storage: window.sessionStorage })] //添加插件
	plugins: [persistedState()]
})
export default store
