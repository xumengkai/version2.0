//getter文件 需要全局的变量通过getter方式获取
const getters = {
	userInfo: (state) => state.user.userInfo,
	token: (state) => state.user.token,
	roles: (state) => state.user.roles,
	menus: (state) => state.user.menus,
	menusSytle: (state) => state.style.menusSytle
}
export default getters
