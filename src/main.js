import { createApp } from 'vue'
import store from '@/store'
import App from './App.vue'
import router from '@/router'
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import * as icons from '@element-plus/icons-vue'
import appConfig from '@utils/base.js'
import '@/assets/css/font.css'
import '@/assets/css/reset.css'
import 'default-passive-events' //解决chrome浏览器事件管理器报错问题

const app = createApp(App)

// 全局注册图标
for (const name in icons) {
	app.component(name, icons[name])
}
app.config.globalProperties.appConfig = appConfig
app
	.use(ElementPlus, {
		locale: zhCn,
		size: 'default',
		zIndex: 1000
	})
	.use(store)
	.use(router)
	.mount('#app')
