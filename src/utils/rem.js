// // 基准大小
// const baseSize = 37.5
// // 设置 rem 函数
// function setRem() {
//     // 当前页面宽度相对于 1920 宽的缩放比例，可根据自己需要修改。
//     const fontSizeNum = document.documentElement.clientWidth / 1920 * baseSize
//     // 设置页面根节点字体大小
//     document.documentElement.style.fontSize = fontSizeNum + 'px'
// }
// // 初始化
// setRem()
// // 改变窗口大小时重新设置 rem
// window.onresize = function () {
//     setRem()
// }
import appConfig from './base'

export function pageResize(callback) {
	let init = () => {
		console.log(window.innerWidth + ',' + window.innerHeight)
		let _el = document.getElementById('app')

		let hScale = window.innerHeight / appConfig.screen.height
		let wScale = window.innerWidth / appConfig.screen.width
		let pageH = window.innerHeight
		let pageW = window.innerWidth

		let isWider = window.innerWidth / window.innerHeight >= appConfig.screen.width / appConfig.screen.height
		console.log(isWider)
		if (isWider) {
			_el.style.height = window.innerHeight + 'px' // '100%';
			_el.style.width = (pageH * appConfig.screen.width) / appConfig.screen.height + 'px'
			_el.style.top = '0px'
			_el.style.left = (window.innerWidth - (pageH * appConfig.screen.width) / appConfig.screen.height) * 0.5 + 'px'
			console.log(_el.style.width + ',' + _el.style.height)
		} else {
			_el.style.width = window.innerWidth + 'px' // '100%';
			_el.style.height = (pageW * appConfig.screen.height) / appConfig.screen.width + 'px'
			_el.style.top = 0.5 * (window.innerHeight - (pageW * appConfig.screen.height) / appConfig.screen.width) + 'px'
			_el.style.left = '0px'
			console.log(_el.style.height)
			console.log(_el.style.top)
		}
		document.documentElement.style.fontSize = _el.clientWidth / appConfig.screen.scale + 'px'
	}
	var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
	window.addEventListener(resizeEvt, init, false)
	document.documentElement.addEventListener('DOMContentLoaded', init, false)
	init()
}
