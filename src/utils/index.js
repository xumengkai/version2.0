const utils = {
	//更换主题
	mix(color1, color2, weight) {
		weight = Math.max(Math.min(Number(weight), 1), 0)
		let r1 = parseInt(color1.substring(1, 3), 16)
		let g1 = parseInt(color1.substring(3, 5), 16)
		let b1 = parseInt(color1.substring(5, 7), 16)
		let r2 = parseInt(color2.substring(1, 3), 16)
		let g2 = parseInt(color2.substring(3, 5), 16)
		let b2 = parseInt(color2.substring(5, 7), 16)
		let r = Math.round(r1 * (1 - weight) + r2 * weight)
		let g = Math.round(g1 * (1 - weight) + g2 * weight)
		let b = Math.round(b1 * (1 - weight) + b2 * weight)
		r = ('0' + (r || 0).toString(16)).slice(-2)
		g = ('0' + (g || 0).toString(16)).slice(-2)
		b = ('0' + (b || 0).toString(16)).slice(-2)
		return '#' + r + g + b
	},
	//echarts自适配 设置px直接用fontSize()包裹原数据即可
	fontSize(res) {
		let docEl = document.documentElement,
			clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
		if (!clientWidth) return
		// 此处的1920 为设计稿的宽度，记得修改！
		let fontSize = clientWidth / 1920
		return res * fontSize
	},
	//message 提示框
	message(type, text, duration) {
		type ? type : null
		if (type !== 'success' && type !== 'warning' && type !== 'info' && type !== 'error') {
			text = type
			type = 'info'
		}
		duration ? duration * 1 : 3000
		ElMessage({
			type: type,
			message: text,
			center: true,
			duration: duration
		})
	},
	//保存token
	localSet(key, value) {
		window.localStorage.setItem(key, JSON.stringify(value))
	},
	//获取token
	localGet(key) {
		const value = window.localStorage.getItem(key)
		try {
			return JSON.parse(window.localStorage.getItem(key))
		} catch (error) {
			return value
		}
	},
	//移除本地存储的token
	localRemove(key) {
		window.localStorage.removeItem(key)
	},
	//替换页面标题
	setPageTitle(pageTitle) {
		const title = '神探'
		if (pageTitle) {
			return `${pageTitle} | ${title}`
		}
		return `${title}`
	},
	//开启全屏
	fullscreen() {
		let el = document.documentElement
		let rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen
		if (typeof rfs != 'undefined' && rfs) {
			rfs.call(el)
		}
		return
	},
	//退出全屏
	exitFull() {
		let exitMethod = document.exitFullscreen || document.mozCancelFullScreen || document.webkitExitFullscreen || document.msExitFullscreen
		if (exitMethod) {
			exitMethod.call(document)
		} else if (typeof window.ActiveXObject !== 'undefined') {
			//for Internet Explorer
			let wscript = new ActiveXObject('WScript.Shell')
			if (wscript !== null) {
				wscript.SendKeys('{F11}')
			}
		}
	}
}

export default utils
