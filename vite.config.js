import { viteMockServe } from 'vite-plugin-mock'
import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import VueSetupExtend from 'vite-plugin-vue-setup-extend'
import envConfig from './config/index' // 环境变量

const env = process.argv[process.argv.length - 1]
const nowEnv = ['preview', 'build'].indexOf(env) > -1 ? envConfig['prod'] : envConfig[env]

export default defineConfig({
	base: './', // 开发或生产环境服务的公共基础路径。
	plugins: [
		vue(),
		VueSetupExtend(),
		AutoImport({
			resolvers: [ElementPlusResolver()]
		}),
		Components({
			resolvers: [
				ElementPlusResolver({
					importStyle: 'sass'
				})
			]
		}),
		viteMockServe({
			mockPath: './src/mock',
			supportTs: false //如果使用 js发开，则需要配置 supportTs 为 false
		})
	],
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: `@use "@/styles/index.scss" as *;`
			}
		},
		postcss: {
			plugins: [require('autoprefixer')]
		}
	},
	resolve: {
		alias: {
			'~': resolve(__dirname, './'),
			'@': resolve(__dirname, './src'), // 设置 `@` 指向 `src` 目录
			'@assets': resolve(__dirname, './src/assets'),
			'@comps': resolve(__dirname, './src/components'),
			'@api': resolve(__dirname, './src/api'),
			'@views': resolve(__dirname, './src/views'),
			'@utils': resolve(__dirname, './src/utils'),
			'@store': resolve(__dirname, './src/store'),
			'@router': resolve(__dirname, './src/router'),
			'@styles': resolve(__dirname, './src/styles')
		}
	},
	server: {
		open: true, //是否自动在浏览器打开
		https: false, //是否开启 https
		host: '0.0.0.0',
		port: nowEnv.port, //端口号
		proxy: nowEnv.domain,
		cors: true, //是否跨域
		ssr: false //服务端渲染
		// hmr: true //热更新
	}
})
