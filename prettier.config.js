//代码规范
module.exports = {
	printWidth: 160, // 换行字符串阈值
	tabWidth: 2, // 设置工具每一个水平缩进的空格数
	useTabs: true, // 是否使用tab进行缩进（默认false）
	singleQuote: true, // 使用单引号（默认false）
	semi: false, // 声明结尾使用分号(默认true)
	vueIndentScriptAndStyle: true,
	trailingComma: 'none', // 最后一个对象元素加逗号
	bracketSpacing: true, // 对象，数组加空格
	jsxBracketSameLine: true, // jsx > 是否另起一行
	arrowParens: 'always', // (x) => {} 是否要有小括号
	requirePragma: false, // 不需要写文件开头的 @prettier
	insertPragma: false, // 不需要自动在文件开头插入 @prettier
	htmlWhitespaceSensitivity: 'ignore' // html格式化 结尾标签不换行
}
